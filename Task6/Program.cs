﻿using System;

namespace Task6
{   
    //Program that calculates and categorizes your BMI
    class BMICalculator
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the BMI calculator!");
            Console.WriteLine("Please enter your weight in kilograms");
                int weight = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Excellent, now enter your height in centimeters");
                int height = Convert.ToInt32(Console.ReadLine());

            double BMI = BMICalculator.calculateBMI(weight, height);
            string category = BMICalculator.categorizeBMI(BMI);
            Console.WriteLine("Your BMI is " + BMI + " which is categorized as " + category);
        }
        //  Calculates BMI
        public static double calculateBMI(int weight, int height)
        {
            double BMI = Math.Round(weight / Math.Pow(height / 100.0, 2), 2);
            return BMI;
        }
        //  Categorize the BMI
        public static string categorizeBMI(double BMI)
        {
            string weightclass = null;
            if (BMI < 18.5)
            {
                weightclass = "underweight";
            }
            else if (18.5 <= BMI && BMI <= 24.9)
            {
                weightclass = "normal weight";
            }
            else if (25 <= BMI && BMI <= 29.9)
            {
                weightclass = "overweight";
            }
            else if (BMI >= 30)
            {
                weightclass = "obese";
            }
            return weightclass;
        }
    }
}
